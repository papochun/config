import subprocess

def read_xresources(prefix):
    props = {}
    x = subprocess.run(['xrdb', '-query'], capture_output=True, check=True, text=True)
    lines = x.stdout.split('\n')
    for line in filter(lambda l : l.startswith(prefix), lines):
        prop, _, value = line.partition(':\t')
        props[prop] = value
    return props

xresources = read_xresources('*')
c.colors.statusbar.normal.bg = xresources['*background']
c.colors.statusbar.normal.fg = xresources['*foreground']

c.colors.tabs.bar.bg = xresources['*background']
c.colors.tabs.even.bg = xresources['*background']
c.colors.tabs.even.fg = xresources['*foreground']
c.colors.tabs.odd.bg = xresources['*background']
c.colors.tabs.odd.fg = xresources['*foreground']

c.colors.tabs.selected.even.bg = xresources['*foreground']
c.colors.tabs.selected.even.fg = xresources['*background']
c.colors.tabs.selected.odd.bg = xresources['*foreground']
c.colors.tabs.selected.odd.fg = xresources['*background']

#c.colors.completion.item.selected.fg

config.load_autoconfig()
